#!/bin/bash
git submodule deinit -f osef-smbus
git rm -f osef-smbus
git rm --cached osef-smbus
rm -rf .git/modules/osef-smbus

git submodule deinit -f osef-gpio
git rm -f osef-gpio
git rm --cached osef-gpio
rm -rf .git/modules/osef-gpio
