#ifndef PMODCOLORLED_H
#define PMODCOLORLED_H

#include "PmodColor.h"
#include "SysOutputGPIO.h"

namespace PMOD { class PmodColorLed : public PmodColor {
public:
    PmodColorLed(const uint8_t& adp, const uint32_t& ledpin);
    virtual ~PmodColorLed();
    
    bool setLedOn() {return led.setHigh();}
    bool setLedOff() {return led.setLow();}
    
private:
    PmodColorLed(const PmodColorLed& orig);
    PmodColorLed& operator=(const PmodColorLed& orig);
    
    OSEF::SysOutputGPIO led;
};}

#endif /* PMODCOLORLED_H */

