#include "PmodColor.h"
#include "Debug.h"

const uint8_t TCS3472_COMMAND = 0x80;
const uint8_t TCS3472_AUTO_INCR = 0x20;

const uint8_t TCS3472_ENABLE = TCS3472_COMMAND + 0x00;
const uint8_t TCS3472_ENABLE_PON = 0x01;
const uint8_t TCS3472_ENABLE_AEN = 0x02;

const uint8_t TCS3472_STATUS  = TCS3472_COMMAND + 0x13;

const uint8_t TCS3472_CDATA = TCS3472_COMMAND + 0x14;
const uint8_t TCS3472_RDATA = TCS3472_COMMAND + 0x16;
const uint8_t TCS3472_GDATA = TCS3472_COMMAND + 0x18;
const uint8_t TCS3472_BDATA = TCS3472_COMMAND + 0x1a;

PMOD::PmodColor::PmodColor(const uint8_t& adp)
    :device(adp, 0x29)
{
    // enable device
    if(!device.writeByte(TCS3472_ENABLE, TCS3472_ENABLE_PON+TCS3472_ENABLE_AEN))
    {
        _DERR("failed to enable device");
    }
}

PMOD::PmodColor::~PmodColor() {}

bool PMOD::PmodColor::getClear(uint16_t &c)
{
    const bool ret = device.readWord(TCS3472_CDATA, c);    
    return ret;
}

bool PMOD::PmodColor::getRed(uint16_t &c)
{
    const bool ret = device.readWord(TCS3472_RDATA, c);    
    return ret;
}

bool PMOD::PmodColor::getGreen(uint16_t &c)
{
    const bool ret = device.readWord(TCS3472_GDATA, c);    
    return ret;
}

bool PMOD::PmodColor::getBlue(uint16_t &c)
{
    const bool ret = device.readWord(TCS3472_BDATA, c);    
    return ret;
}

bool PMOD::PmodColor::getStatus(uint8_t& s)
{
    const bool ret = device.readByte(TCS3472_STATUS, s);    
    return ret;
}
