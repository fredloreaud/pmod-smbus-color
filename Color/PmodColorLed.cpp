#include "PmodColorLed.h"
#include "Debug.h"

PMOD::PmodColorLed::PmodColorLed(const uint8_t& adp, const uint32_t& ledpin)
    :PmodColor(adp)
    ,led(ledpin)
{
    if(!led.setHigh())
    {
        _DERR("failed to set LED "<<ledpin<<" ON");
    }
    else
    {
        bool state = false;
        if(led.getValue(state))
        {
            if(state)
            {
                _DOUT("LED "<<ledpin<<" is ON");
            }
            else
            {
                _DOUT("LED "<<ledpin<<" is OFF");
            }
        }
        else
        {
            _DERR("failed to read LED "<<ledpin<<" state");
        }
    }
}

PMOD::PmodColorLed::~PmodColorLed()
{
    if(!led.setLow())
    {
        _DERR("failed to set led OFF");
    }
}

