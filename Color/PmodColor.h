#ifndef PMODCOLOR_H
#define PMODCOLOR_H

#include "SMBusDevice.h"

// https://reference.digilentinc.com/reference/pmod/pmodcolor/reference-manual
// http://ams.com/eng/Products/Light-Sensors/Color-Sensors/TCS34725
// https://github.com/torvalds/linux/blob/master/drivers/iio/light/tcs3472.c
// https://elixir.bootlin.com/linux/latest/source/drivers/iio/light/tcs3472.c

namespace PMOD { class PmodColor ///< Diligent Pmod Color with TCS34725
{
public:
    explicit PmodColor(const uint8_t& adp); ///<  device 0x29
    virtual ~PmodColor();
    
    bool getClear(uint16_t& c);
    bool getRed(uint16_t& c);
    bool getGreen(uint16_t& c);
    bool getBlue(uint16_t& c);
    
    bool getStatus(uint8_t& s);
    
private:
    PmodColor(const PmodColor& orig);
    PmodColor& operator=(const PmodColor& orig);
    
    OSEF::SMBusDevice device;
};}

#endif /* PMODCOLOR_H */

